[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)

Add the following linker flags:
```
-ObjC
-l"c++"
-l"z"
-framework
"AVFoundation"
-framework
"Accelerate"
-framework
"CoreGraphics"
-framework
"CoreMedia"
-framework
"CoreVideo"
-framework
"AddressBook"
```
